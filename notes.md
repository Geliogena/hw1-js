Variables are used to store information that the programmer will use during the development of the project.
Variable declaration is the process of assigning a name to a variable.
Variables can be declared in 3 ways: 
1. Using the “var” command, for example: var userName. This method is considered obsolete in JavaScript
   and can be used in older developments.
2. The second way to declare a variable is to use the "let" command, for example: let userName.
   or the third method, the const command is used, and then the name that is assigned to the variable. In this case, you
3. need to immediately indicate the value of the variable, since it can be assigned only once in the project. 
   The variable must be initialized immediately. For example: const userName = "Roman".

In JavaScript, any text data is a string. You can create a string using single, double, or backticks. For example:
const single = 'single-quoted';
let double = "double-quoted";
const backticks = `backticks`;

To check the data "typeof" a variable in JavaScript, you need to make a request using the "typeof" command and print the 
image to the console. For example: console.log(typeof userName).

When using the mathematical addition operator "+", you can get the result 1+"1"=11 if one of the operands is a numeric
data type and the other is a string data type, which is quoted when assigning them to a variable.